﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;
using System.Runtime.InteropServices;

namespace JsonViewer
{


    public partial class Form1 : Form
    {
        public JsonConfig json = new JsonConfig();
        Settings set = new Settings();
        public string filePath = "settings.json";
        
        public Form1()
        {
            InitializeComponent();
            Initalise();

        }
        private void Initalise()
        {
            // Read in settings from file
            set = json.Read<Settings>(filePath);

            // Initalise Settings Values 

            // Sound
            numGlobalVolume.Text = set.GlobalVolume.ToString();
            numSoundFX.Value = set.SoundFX;
            
            //Graphics
            // Add current resolution to top of combo box
            cboScreenResolution.Items.Add(set.ScreenWidth.ToString() + "x" + set.ScreenHeight.ToString());
            cboScreenResolution.SelectedIndex = 0;
            
            // Set mode
            if(set.Windowed)
            {
                rdoWindowed.Checked = true;
                rdoFullscreen.Checked = false;
            }
        }

        private void btRead_Click(object sender, EventArgs e)
        {
            set = json.Read<Settings>(filePath);
            UpdateValues();
        }

        private void UpdateValues()
        {
            numGlobalVolume.Text = set.GlobalVolume.ToString();
            numSoundFX.Value = set.SoundFX;
            cbTextureLevel.SelectedItem = set.TextureLevel;
            //cboScreenResolution.SelectedText = set.ScreenHeight.ToString() + "x" + set.ScreenWidth.ToString();
            rdoWindowed.Checked = set.Windowed;
            rdoFullscreen.Checked = set.Fulscreen;
        }

        private void btWrite_Click(object sender, EventArgs e)
        {
            set.Windowed = rdoWindowed.Checked;
            set.Fulscreen = rdoFullscreen.Checked;
            set.GlobalVolume = Convert.ToInt32(numGlobalVolume.Value);
            set.SoundFX = Convert.ToInt32(numSoundFX.Value);

            //string width = cboScreenResolution.SelectedItem.ToString();
            //width = width.Substring(0, width.IndexOf('x'));
            //set.ScreenWidth = Convert.ToInt32(width);

            //string height = cboScreenResolution.SelectedItem.ToString();
            //height = height.Substring( width.IndexOf('x'), width.Length);
            //set.ScreenHeight = Convert.ToInt32(height);

            set.TextureLevel = cbTextureLevel.SelectedText;
            MessageBox.Show(cbTextureLevel.SelectedIndex.ToString());

            json.Write(filePath, set);
        }
    }
    public class JsonConfig
    {
        public JsonConfig(){}
        public void Write(string filePath,object ob ){
            string jsonText = Newtonsoft.Json.JsonConvert.SerializeObject(ob);
            File.WriteAllText(filePath, jsonText);
        }
        public T Read<T>(string filePath)
        {
            // Read file 
            string file = File.ReadAllText(filePath);
            // Read json file into object
            return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(file);
        }
    }
    public class Settings
    {
        public int ScreenWidth;
        public int ScreenHeight;
        public int GlobalVolume;
        public int SoundFX;
        public int Music;
        public bool Windowed;
        public bool Fulscreen;
        public string TextureLevel;
        public Settings()
        {
            ScreenWidth= 1920;
            ScreenHeight = 1080;
            GlobalVolume = 100;
            SoundFX = 75;
            Music = 10;
            Windowed = false;
            Fulscreen = true;
            TextureLevel = "High";
        }

        
    }
}
