﻿namespace JsonViewer
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.cboScreenResolution = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btRead = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.btWrite = new System.Windows.Forms.Button();
            this.numSoundFX = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.rdoWindowed = new System.Windows.Forms.RadioButton();
            this.rdoFullscreen = new System.Windows.Forms.RadioButton();
            this.cbTextureLevel = new System.Windows.Forms.ComboBox();
            this.lbTextureLevel = new System.Windows.Forms.Label();
            this.numGlobalVolume = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.numSoundFX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numGlobalVolume)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(9, 77);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Screen Resolution";
            // 
            // cboScreenResolution
            // 
            this.cboScreenResolution.FormattingEnabled = true;
            this.cboScreenResolution.Location = new System.Drawing.Point(8, 103);
            this.cboScreenResolution.Name = "cboScreenResolution";
            this.cboScreenResolution.Size = new System.Drawing.Size(417, 21);
            this.cboScreenResolution.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(5, 297);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(83, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Global Volume";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(7, 350);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(98, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Sound FX Volume";
            // 
            // btRead
            // 
            this.btRead.Location = new System.Drawing.Point(350, 67);
            this.btRead.Name = "btRead";
            this.btRead.Size = new System.Drawing.Size(75, 23);
            this.btRead.TabIndex = 5;
            this.btRead.Text = "Read";
            this.btRead.UseVisualStyleBackColor = true;
            this.btRead.Click += new System.EventHandler(this.btRead_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(67, 9);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(250, 45);
            this.label4.TabIndex = 6;
            this.label4.Text = "Settings Config";
            // 
            // btWrite
            // 
            this.btWrite.Location = new System.Drawing.Point(267, 67);
            this.btWrite.Name = "btWrite";
            this.btWrite.Size = new System.Drawing.Size(75, 23);
            this.btWrite.TabIndex = 7;
            this.btWrite.Text = "Write";
            this.btWrite.UseVisualStyleBackColor = true;
            this.btWrite.Click += new System.EventHandler(this.btWrite_Click);
            // 
            // numSoundFX
            // 
            this.numSoundFX.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numSoundFX.Location = new System.Drawing.Point(8, 366);
            this.numSoundFX.Name = "numSoundFX";
            this.numSoundFX.Size = new System.Drawing.Size(120, 22);
            this.numSoundFX.TabIndex = 8;
            this.numSoundFX.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold);
            this.label5.Location = new System.Drawing.Point(8, 54);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(76, 21);
            this.label5.TabIndex = 9;
            this.label5.Text = "Graphics";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold);
            this.label6.Location = new System.Drawing.Point(4, 265);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(66, 21);
            this.label6.TabIndex = 10;
            this.label6.Text = "Sounds";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold);
            this.label7.Location = new System.Drawing.Point(251, 181);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(74, 21);
            this.label7.TabIndex = 11;
            this.label7.Text = "Controls";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(9, 127);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(74, 13);
            this.label8.TabIndex = 12;
            this.label8.Text = "Screen Mode";
            // 
            // rdoWindowed
            // 
            this.rdoWindowed.AutoSize = true;
            this.rdoWindowed.Location = new System.Drawing.Point(12, 143);
            this.rdoWindowed.Name = "rdoWindowed";
            this.rdoWindowed.Size = new System.Drawing.Size(82, 17);
            this.rdoWindowed.TabIndex = 13;
            this.rdoWindowed.TabStop = true;
            this.rdoWindowed.Text = "Windowed";
            this.rdoWindowed.UseVisualStyleBackColor = true;
            // 
            // rdoFullscreen
            // 
            this.rdoFullscreen.AutoSize = true;
            this.rdoFullscreen.Location = new System.Drawing.Point(12, 166);
            this.rdoFullscreen.Name = "rdoFullscreen";
            this.rdoFullscreen.Size = new System.Drawing.Size(77, 17);
            this.rdoFullscreen.TabIndex = 14;
            this.rdoFullscreen.TabStop = true;
            this.rdoFullscreen.Text = "Fullscreen";
            this.rdoFullscreen.UseVisualStyleBackColor = true;
            // 
            // cbTextureLevel
            // 
            this.cbTextureLevel.FormattingEnabled = true;
            this.cbTextureLevel.Items.AddRange(new object[] {
            "Fantastic",
            "High",
            "Medium",
            "Low"});
            this.cbTextureLevel.Location = new System.Drawing.Point(7, 205);
            this.cbTextureLevel.Name = "cbTextureLevel";
            this.cbTextureLevel.Size = new System.Drawing.Size(121, 21);
            this.cbTextureLevel.TabIndex = 15;
            // 
            // lbTextureLevel
            // 
            this.lbTextureLevel.AutoSize = true;
            this.lbTextureLevel.Location = new System.Drawing.Point(9, 189);
            this.lbTextureLevel.Name = "lbTextureLevel";
            this.lbTextureLevel.Size = new System.Drawing.Size(77, 13);
            this.lbTextureLevel.TabIndex = 16;
            this.lbTextureLevel.Text = "Texture Detail";
            // 
            // numGlobalVolume
            // 
            this.numGlobalVolume.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numGlobalVolume.Location = new System.Drawing.Point(7, 313);
            this.numGlobalVolume.Name = "numGlobalVolume";
            this.numGlobalVolume.Size = new System.Drawing.Size(120, 22);
            this.numGlobalVolume.TabIndex = 17;
            this.numGlobalVolume.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(437, 401);
            this.Controls.Add(this.numGlobalVolume);
            this.Controls.Add(this.lbTextureLevel);
            this.Controls.Add(this.cbTextureLevel);
            this.Controls.Add(this.rdoFullscreen);
            this.Controls.Add(this.rdoWindowed);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.numSoundFX);
            this.Controls.Add(this.btWrite);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btRead);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cboScreenResolution);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.numSoundFX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numGlobalVolume)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cboScreenResolution;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btRead;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btWrite;
        private System.Windows.Forms.NumericUpDown numSoundFX;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.RadioButton rdoWindowed;
        private System.Windows.Forms.RadioButton rdoFullscreen;
        private System.Windows.Forms.ComboBox cbTextureLevel;
        private System.Windows.Forms.Label lbTextureLevel;
        private System.Windows.Forms.NumericUpDown numGlobalVolume;
    }
}

